package entity.station;

import entity.bike.Bike;
import entity.db.EcoRentalBikeDB;

import java.sql.SQLException;
import java.sql.Statement;

public class StationBike {

    private Bike bike;
    protected Statement stm;
    protected int id;
    protected String name;
    protected String location;
    protected int numOfEmptyDockingPoint;
    protected String availableStandardBike;
    protected String availableEBike;
    protected String availableTwinBike;
    protected String distance;
    protected String walkingTime;

    public StationBike() throws SQLException {
        stm = EcoRentalBikeDB.getConnection().createStatement();
    }

    public StationBike(Bike bike, Statement stm, int id, String name, String location, int numOfEmptyDockingPoint, String availableStandardBike,
                    String availableEBike, String availableTwinBike, String distance, String walkingTime) {
        this.bike = bike;
        this.stm = stm;
        this.id = id;
        this.name = name;
        this.location = location;
        this.numOfEmptyDockingPoint = numOfEmptyDockingPoint;
        this.availableStandardBike = availableStandardBike;
        this.availableEBike = availableEBike;
        this.availableTwinBike = availableTwinBike;
        this.distance = distance;
        this.walkingTime = walkingTime;
    }

    // Getter and setter

    public Bike getBike() { return bike; }

    public void setBike(Bike bike) { this.bike = bike; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getLocation() { return location; }

    public void setLocation(String location) { this.location = location; }

    public int getNumOfEmptyDockingPoint() { return numOfEmptyDockingPoint; }

    public void setNumOfEmptyDockingPoint(int numOfEmptyDockingPoint) { this.numOfEmptyDockingPoint = numOfEmptyDockingPoint; }

    public String getAvailableStandardBike() { return availableStandardBike; }

    public void setAvailableStandardBike(String availableStandardBike) { this.availableStandardBike = availableStandardBike; }

    public String getAvailableEBike() { return availableEBike; }

    public void setAvailableEBike(String availableEBike) { this.availableEBike = availableEBike; }

    public String getAvailableTwinBike() { return availableTwinBike; }

    public void setAvailableTwinBike(String availableTwinBike) { this.availableTwinBike = availableTwinBike; }

    public String getDistance() { return distance; }

    public void setDistance(String distance) { this.distance = distance; }

    public String getWalkingTime() { return walkingTime; }

    public void setWalkingTime(String walkingTime) { this.walkingTime = walkingTime; }

    @Override

    public String toString() {
        return "{" +
                " bike='" + bike + "'" +
                " id='" + id + "'" +
                ", name='" + name + "'" +
                ", location='" + location + "'" +
                ", numOfEmptyDockingPoint='" + numOfEmptyDockingPoint + "'" +
                ", availableStandardBike='" + availableStandardBike + "'" +
                ", availableEBike='" + availableEBike + "'" +
                ", availableTwinBike='" + availableTwinBike + "'" +
                ", distance='" + distance + "'" +
                ", walkingTime='" + walkingTime + "'" +
                "}";
    }


}

