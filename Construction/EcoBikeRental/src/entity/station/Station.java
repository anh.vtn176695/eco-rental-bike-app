package entity.station;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import common.exception.BikeNotAvailableException;
import common.exception.FullDockingPointException;
import entity.bike.Bike;
import module.Database;

public class Station {

    private List<StationBike> lstStationBike;
    private static Station stationInstance;

    public static Station getStation() {
        if (stationInstance == null) stationInstance = new Station();
        return stationInstance;
    }

    public Station(){}

    public int getStationNow() throws SQLException, ClassNotFoundException {
        Connection connection = new Database().getConnection();
        Statement statement = connection.createStatement();
        String sql = "select * from selectStation";
        ResultSet resultSet = statement.executeQuery(sql);
        int id=0;
        while (resultSet.next()){
            id = resultSet.getInt("bike");
        }
        statement.close();
        return id;
    }
//    public void addStationBike(StationBike sb) { lstStationBike.add(sb); }
//
//    public void removeStationBike(StationBike sb) { lstStationBike.remove(sb); }
//
//    public List getListBike() { return lstStationBike; }
//
//    public void emptyStation() { lstStationBike.clear(); }
//
//    public int getTotalBike() {
//        int total = 0;
//        for (Object obj : lstStationBike) {
//            StationBike sb = (StationBike) obj;
//            total += sb.getTime();
//        }
//        return total;
//    }
//
//    public int calSubtotal() {
//        int total = 0;
//        for (Object obj : lstStationBike) {
//            StationBike sb = (StationBike) obj;
//            total += sb.getPrice()*getQuantity();
//        }
//        return total;
//    }

//    public void checkAvailabilityOfBike() throws SQLException {
//        boolean allAvai = true;
//        for (Object object : lstStationBike) {
//            StationBike stationBike = (StationBike) object;
//            int requiredTime = stationBike.getQuantity();
//            int availQuantity = stationBike.getBike().getQuantity();
//            if (requiredQuantity > availQuantity) allAvai = false;
//        }
//        if (!allAvai) throw new BikeNotAvailableException("The bike not available");
//    }

    public StationBike checkBikeInStation(Bike bike) {
        for (StationBike stationBike : lstStationBike) {
            if (stationBike.getBike().getId() == bike.getId()) return stationBike;
        }
        return null;
    }

    public void checkEmptyDockingPoint() throws SQLException {
        boolean allEmpty = true;
        for (StationBike stationBike : lstStationBike) {
            if (stationBike.getNumOfEmptyDockingPoint() < 0) throw new FullDockingPointException("The Station is full docking points");
        }
    }

}
