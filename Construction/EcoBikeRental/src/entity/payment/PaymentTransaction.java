package entity.payment;

public class PaymentTransaction {

    private String errorCode;
    private CreditCard card;
    private String id;
    private String content;
    private int amount;
    private String createdAt;

    public PaymentTransaction(String errorCode, CreditCard card, String id,
                              String content, int amount, String createdAt) {
        super();
        this.errorCode = errorCode;
        this.card = card;
        this.id = id;
        this.content = content;
        this.amount = amount;
        this.createdAt = createdAt;
    }

    public String getErrorCode() { return errorCode; }
}
