package entity.returnbike;

import entity.bike.Bike;
import module.Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalTime;
import java.util.Date;

public class ReturnBike {

    private Bike bike;
    private String barcode;
    private String type;
    private String deposit;
    private String price;
    private LocalTime returnTime;


    public ReturnBike(){

    }


    public ReturnBike(Bike bike, String barcode, String type, String deposit, String price, LocalTime returnTime) {
        this.bike = bike;
        this.barcode = barcode;
        this.type = type;
        this.deposit = deposit;
        this.price = price;
        this.returnTime = returnTime;

    }

    @Override
    public String toString() {
        return "{" +
                " bike='" + bike + "'" +
                " barcode='" + barcode + "'" +
                " type='" + type + "'" +
                " deposit='" + deposit + "'" +
                " price='" + price + "'" +
                " returnTime='" + returnTime + "'" +
                "}";
    }

    public boolean checkReturnBarcode(String barcode) throws SQLException, ClassNotFoundException {
        Connection connection = new Database().getConnection();
        Statement statement = connection.createStatement();
        String sql = "select * from rentBike where barcode = '"+barcode+"'";
        ResultSet resultSet = statement.executeQuery(sql);
        if (resultSet.next()) {return true;}
        return false;
    }


    public Bike getBike() { return this.bike; }

    public void setBike(Bike bike) { this.bike = bike; }

    public String getBarcode() { return barcode; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public void setBarcode(String barcode) { this.barcode = barcode; }

    public String getDeposit() { return deposit; }

    public void setDeposit(String deposit) { this.deposit = deposit; }

    public String getPrice() { return price; }

    public void setPrice(String price) { this.price = price; }

    public LocalTime getReturnTime() {
        returnTime = java.time.LocalTime.now();
        return returnTime; }

    public void setReturnTime(LocalTime returnTime) { this.returnTime = returnTime; }

}
