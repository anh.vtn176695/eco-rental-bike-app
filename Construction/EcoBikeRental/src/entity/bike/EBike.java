package entity.bike;

import entity.db.EcoRentalBikeDB;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import module.Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class EBike extends Bike{

//    protected int id;
//    protected int bikeId;
    protected String batteryPercentage;
    protected String leftTime;

    public EBike() throws SQLException {
    }

//    public EBike(Statement stm, int id, String type, String saddle, String barcode, String deposit,
//                 String rearSeat, String pedal, int stationId) {
//        super(stm, id, type, saddle, barcode, deposit, rearSeat, pedal, stationId);
//    }
//
//    public EBike(int id, int bikeId, String batteryPercentage, String leftTime) throws SQLException {
////        this.id = id;
////        this.bikeId = bikeId;
//        this.batteryPercentage = batteryPercentage;
//        this.leftTime = leftTime;
//    }

    public EBike(Statement stm, int id, String type, String saddle, String barcode, String deposit, String rearSeat, String pedal, int stationId, String batteryPercentage, String leftTime){
        super(stm, id, type, saddle, barcode, deposit, rearSeat, pedal, stationId);
        this.batteryPercentage = batteryPercentage;
        this.leftTime = leftTime;
    }
    // Getter and setter


    @Override
    public int getId() { return id; }

    @Override
    public void setId(int id) { this.id = id; }

//    public int getBikeId() { return bikeId; }
//
//    public void setBikeId(int bikeId) { this.bikeId = bikeId; }

    public String getBatteryPercentage() { return batteryPercentage; }

    public void setBatteryPercentage(String batteryPercentage) { this.batteryPercentage = batteryPercentage; }

    public String getLeftTime() { return leftTime; }

    public void setLeftTime(String leftTime) { this.leftTime = leftTime; }


    public ObservableList<EBike> getAllEBikeByStationId(int id) throws SQLException, ClassNotFoundException {
        ObservableList<EBike> list = FXCollections.observableArrayList();
        Connection connection = new Database().getConnection();
        Statement statement = connection.createStatement();
        String sql = "select * from EBike join Bike on EBike.bikeId = Bike.id where Bike.stationId ='"+id+"'";
        ResultSet resultSet = statement.executeQuery(sql);
        int bikeId=0;
        while (resultSet.next()){
//            int bikeId = resultSet.getInt(1);
            bikeId++;
            String batteryPercentage = resultSet.getString(3);
            String leftTime = resultSet.getString(4);
            String type = resultSet.getString(6);
            String saddle = resultSet.getString(7);
            String barcode = resultSet.getString(9);
            String deposit = resultSet.getString(10);
            String rearSeat = resultSet.getString(11);
            String pedal = resultSet.getString(8);
            int stationId = resultSet.getInt(12);

            Statement stm = EcoRentalBikeDB.getConnection().createStatement();
            list.add(new EBike(stm, bikeId, type,saddle, barcode, deposit, rearSeat,pedal, stationId, batteryPercentage, leftTime));
        }
        return list;
    }

    public EBike getEBikeByBarcode(String barcode) throws SQLException, ClassNotFoundException {
        EBike bike = new EBike();
        Connection connection = new Database().getConnection();
        Statement statement = connection.createStatement();
        String sql = "select * from ebike join bike on ebike.bikeId = bike.id where barcode='"+barcode+"'";
        ResultSet resultSet = statement.executeQuery(sql);
        if (resultSet.next()) {
            bike.setId(resultSet.getInt(1));
            bike.setBarcode(barcode);
            bike.setDeposit(resultSet.getString("deposit"));
            bike.setRearSeat(resultSet.getString("rearSeat"));
            bike.setType("E-Bike");
            bike.setSaddle(resultSet.getString("saddle"));
            bike.setPedal(resultSet.getString("pedal"));
            bike.setBatteryPercentage(resultSet.getString("batteryPercentage"));
            bike.setLeftTime(resultSet.getString("leftTime"));
        }
        return bike;
    }
}
