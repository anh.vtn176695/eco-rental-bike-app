package entity.bike;

import entity.db.EcoRentalBikeDB;
import module.Database;
import utils.Utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * The general bike class, for another bike it can be done by inheriting this class
 * @author Anhvtn
 */

public class Bike {

    private static Logger LOGGER = Utils.getLogger(Bike.class.getName());
    protected Statement stm;
    protected int id;
    protected String type;
    protected String saddle;
    protected String barcode;
    protected String deposit;
    protected String rearSeat;
    protected String pedal;
    protected int stationId;

    public Bike() throws SQLException {
        stm = EcoRentalBikeDB.getConnection().createStatement();
    }

    public Bike(Statement stm, int id, String type, String saddle, String barcode,
                String deposit, String rearSeat, String pedal, int stationId) {
        this.stm = stm;
        this.id = id;
        this.type = type;
        this.saddle = saddle;
        this.barcode = barcode;
        this.deposit = deposit;
        this.rearSeat = rearSeat;
        this.pedal = pedal;
        this.stationId = stationId;
    }


    // getter and setter

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public String getSaddle() { return saddle; }

    public void setSaddle(String saddle) { this.saddle = saddle; }

    public String getBarcode() { return barcode; }

    public void setBarcode(String barcode) { this.barcode = barcode; }

    public String getDeposit() { return deposit; }

    public void setDeposit(String deposit) { this.deposit = deposit; }

    public String getRearSeat() { return rearSeat; }

    public void setRearSeat(String rearSeat) { this.rearSeat = rearSeat; }

    public String getPedal() { return pedal; }

    public void setPedal(String pedal) { this.pedal = pedal; }

    public int getStationId() { return stationId; }

    public void setStationId(int stationId) { this.stationId = stationId; }

    @Override
    public String toString() {
        return "{" +
                " id='" + id + "'" +
                ", type='" + type + "'" +
                ", saddle='" + saddle + "'" +
                ", barcode='" + barcode + "'" +
                ", deposit='" + deposit + "'" +
                ", rearSeat='" + rearSeat + "'" +
                ", pedal='" + pedal + "'" +
                ", stationId='" + stationId + "'" +
                "}";
    }

}

