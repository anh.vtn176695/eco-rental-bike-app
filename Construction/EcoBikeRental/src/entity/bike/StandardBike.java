package entity.bike;

//import entity.db.EcoRentalBikeDB;

import module.Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class StandardBike extends Bike {

//    protected int id;
//    protected int bikeId;
//
    public StandardBike() throws SQLException {

    }

    public StandardBike(Statement stm, int id, String type, String saddle, String barcode,
                        String deposit, String rearSeat, String pedal, int stationId) {
        super(stm, id, type, saddle, barcode, deposit, rearSeat, pedal, stationId);
    }


    // getter and setter

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

//    public int getBikeId() { return bikeId; }
//
//    public void setBikeId(int bikeId) { this.bikeId = bikeId; }


    public StandardBike getBikeByBarcode(String barcode) throws SQLException, ClassNotFoundException {
        StandardBike bike = new StandardBike();
        Connection connection = new Database().getConnection();
        Statement statement = connection.createStatement();
        String sql = "select * from Bike where barCode='" + barcode + "'";
        ResultSet resultSet = statement.executeQuery(sql);
        if (resultSet.next()) {
            bike.setId(resultSet.getInt(1));
            bike.setBarcode(barcode);
            bike.setType("Standard Bike");
            bike.setDeposit(resultSet.getString(6));
            bike.setPedal(resultSet.getString(4));
            bike.setSaddle(resultSet.getString(3));
            bike.setRearSeat(resultSet.getString(7));
        }
        return bike;
    }
}