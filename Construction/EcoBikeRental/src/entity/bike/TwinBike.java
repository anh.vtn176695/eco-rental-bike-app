package entity.bike;

import entity.db.EcoRentalBikeDB;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import module.Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TwinBike extends Bike {

//    protected int id;
//    protected int bikeId;

    private String leftTime;
    private String batteryPercentage;

    public TwinBike() throws SQLException {
    }

//    public TwinBike(int id, int bikeId) throws SQLException {
//        this.id = id;
//        this.bikeId = bikeId;
//    }

    public TwinBike(Statement stm, int id, String type, String saddle, String barcode,
                    String deposit, String rearSeat, String pedal, int stationId) {
        super(stm, id, type, saddle, barcode, deposit, rearSeat, pedal, stationId);
    }

    // Getter and setter


//    @Override
//    public int getId() { return id; }
//
//    @Override
//    public void setId(int id) { this.id = id; }

//    public int getBikeId() { return bikeId; }
//
//    public void setBikeId(int bikeId) { this.bikeId = bikeId; }

    public ObservableList<TwinBike> getAllTwinBike(int id) throws SQLException, ClassNotFoundException {
        ObservableList<TwinBike> list = FXCollections.observableArrayList();
        Connection connection = new Database().getConnection();
        Statement statement = connection.createStatement();
        String sql = "select * from twinbike join Bike on twinbike.bikeId = Bike.id where Bike.stationId ='"+id+"'";
        ResultSet resultSet = statement.executeQuery(sql);
        int bikeId=0;
        while (resultSet.next()){
            bikeId ++;
            String type = resultSet.getString(4);
            String saddle = resultSet.getString(5);
            String barcode = resultSet.getString(7);
            String deposit = resultSet.getString(8);
            String rearSeat = resultSet.getString(9);
            String pedal = resultSet.getString(6);
            int stationId = resultSet.getInt(10);

            Statement stm = EcoRentalBikeDB.getConnection().createStatement();
//            (Statement stm, int id, String type, String saddle, String barcode,
//                    String deposit, String rearSeat, String pedal, int stationId
            list.add(new TwinBike(stm, bikeId, type,saddle, barcode, deposit, rearSeat,pedal, stationId));
        }
        return list;
    }

    public TwinBike getTwinBikeByBarcode(String barcode) throws SQLException, ClassNotFoundException {
        TwinBike bike = new TwinBike();
        Connection connection = new Database().getConnection();
        Statement statement = connection.createStatement();
        String sql = "select * from twinbike join bike on twinbike.bikeId = bike.id where barcode='"+barcode+"'";
        ResultSet resultSet = statement.executeQuery(sql);
        if (resultSet.next()) {
            bike.setId(resultSet.getInt(1));
            bike.setBarcode(barcode);
            bike.setDeposit(resultSet.getString("deposit"));
            bike.setRearSeat(resultSet.getString("rearSeat"));
            bike.setType("Twin Bike");
            bike.setSaddle(resultSet.getString("saddle"));
            bike.setPedal(resultSet.getString("pedal"));
        }
        return bike;
    }
}
