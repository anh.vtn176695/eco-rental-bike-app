package entity.db;

import utils.Utils;

import java.sql.Connection;

import java.sql.DriverManager;
import java.util.logging.Logger;

public class EcoRentalBikeDB {

    private static Logger LOGGER = Utils.getLogger(Connection.class.getName());
    private static Connection connect;

    public static Connection getConnection() {
        if (connect != null) return connect;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/itss";
            connect = DriverManager.getConnection(url,"root", "");
            LOGGER.info("Connect database successfully");
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
        }
        return connect;
    }

    public static void main(String[] args) {
        EcoRentalBikeDB.getConnection();
    }

}
