package entity.user;

public class User {
    private int id;
    private String name;
    private String email;
    private String phoneNumber;
    private String city;

    public User(int id, String name, String email,
                String phoneNumber, String city) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.city = city;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getPhoneNumber() { return phoneNumber; }

    public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber; }

    public String getCity() { return city; }

    public void setCity(String city) { this.city = city; }

    @Override
    public String toString() {
        return "{" +
                " id='" + id + "'" +
                " username='" + name + "'" +
                ", email='" + email + "'" +
                ", phonenumber='" + phoneNumber + "'" +
                ", city='" + city + "'" +
                "}";
    }

}
