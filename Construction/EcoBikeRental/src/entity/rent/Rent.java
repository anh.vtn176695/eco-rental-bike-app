package entity.rent;

public class Rent {

    private String price;
    private String deposit;

    public Rent() {
    }

    public Rent(String price, String deposit) {
        this.price = price;
        this.deposit = deposit;
    }

    public String getPrice() { return price; }

    public void setPrice(String price) { this.price = price; }

    public String getDeposit() { return deposit; }

    public void setDeposit(String deposit) { this.deposit = deposit; }

}

