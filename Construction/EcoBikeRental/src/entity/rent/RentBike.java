package entity.rent;

import entity.bike.Bike;
import entity.bike.EBike;
import entity.db.EcoRentalBikeDB;
import module.Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalTime;
import java.util.Date;

public class RentBike {

    private Bike bike;
    private String barcode;
    private String type;
    private String deposit;
    private String price;
    public LocalTime rentTime;

    public RentBike(){

    }
    public RentBike(Bike bike, String barcode, String type, String deposit, String price, LocalTime rentTime) {
        this.bike = bike;
        this.barcode = barcode;
        this.type = type;
        this.deposit = deposit;
        this.price = price;
        this.rentTime = rentTime;
    }

    @Override
    public String toString() {
        return "{" +
                " bike='" + bike + "'" +
                " barcode='" + barcode + "'" +
                " type='" + type + "'" +
                " deposit='" + deposit + "'" +
                " price='" + price + "'" +
                " rentTime='" + rentTime + "'" +
                "}";
    }

    public boolean validateRentBikeBarcode(String barcode) throws SQLException, ClassNotFoundException {
        Connection connection = new EcoRentalBikeDB().getConnection();
        Statement statement = connection.createStatement();
        String sql = "select * from Bike where barCode = '"+barcode+"'";
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()){
            return true;
        }
        return false;
    }

    public String getTypeOfBikeByBarcode(String barcode) throws SQLException, ClassNotFoundException {
        Connection connection  = new Database().getConnection();
        Statement statement = connection.createStatement();
        String sql = "select * from Bike where barcode = '"+barcode+"'";
        String type="";
        ResultSet resultSet = statement.executeQuery(sql);
        if (resultSet.next()){
            type =resultSet.getString("type");
        }
        return type;
    }

    public Bike getBike() { return this.bike; }

    public void setBike(Bike bike) { this.bike = bike; }

    public String getBarcode() { return barcode; }

    public void setBarcode(String barcode) { this.barcode = barcode; }

    public String getDeposit() { return deposit; }

    public void setDeposit(String deposit) { this.deposit = deposit; }

    public String getPrice() { return price; }

    public void setPrice(String price) { this.price = price; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public LocalTime getRentTime() {
        rentTime = java.time.LocalTime.now();
        return rentTime; }

    public void setRentTime(LocalTime rentTime) { this.rentTime = rentTime; }
}
