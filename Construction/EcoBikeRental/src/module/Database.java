package module;

import java.sql.*;

public class Database {
    private Connection  connection;

    public Database() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/itss", "root", "");
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection con;
        Class.forName("com.mysql.cj.jdbc.Driver");
        con = DriverManager.getConnection("jdbc:mysql://localhost:3306/itss", "root", "");
        return con;
    }


    public void shutdown() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    public int addCustomer(String name, String city, String location, Date birth) throws SQLException {
        int id=-1;
        int re;
        String sql = "insert into Customer(fullName, city,currentLocation,dateOfBirth) values('"+name+"', '"+city+"', '"+location+"','"+birth+"')";
        Statement st = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        re = st.executeUpdate(sql ,Statement.RETURN_GENERATED_KEYS);

        ResultSet resultSet = st.getGeneratedKeys();
        if(resultSet.next()){
            id = resultSet.getInt(1);
        }
        st.close();
        return id;

    }

    public void addAccount(String email, String userName, String password, String phone, int cusID) throws SQLException {
        Statement st = connection.createStatement();
        String sql="insert into Account(email,userName,password,phoneNumber,customerId) values('"+email+"','"+userName+"','"+password+"','"+phone+"','"+cusID+"')";
        st.execute(sql);
        st.close();
    }

    public boolean validateAccount(String user, String pass) throws SQLException {
        String sql = "select * from Account where userName='"+user+"' and password='"+pass+"'";
        Statement st = connection.createStatement();
        ResultSet resultSet = st.executeQuery(sql);

        if (resultSet.next()) {
            st.close();
            return true;
        }
        st.close();
        return false;
    }

    public String getNameCustomer(String user, String pass) throws SQLException {
        String sql ="select fullName from Customer join Account on Customer.id = Account.customerId where Account.userName='"+user+"' and password='"+pass+"'";
        Statement st = connection.createStatement();
        ResultSet resultSet = st.executeQuery(sql);

        String fname="";
        if (resultSet.next()){
            fname = resultSet.getString(1);
            System.out.println(fname);
        }
        return fname;
    }
}
