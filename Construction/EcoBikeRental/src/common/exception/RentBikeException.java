package common.exception;

/**
 * The RentBikeException wraps all unchecked exceptions You can use this exception to inform
 */

public class RentBikeException extends EcoRentalBikeException {

    private static final long serialVersionUID = 1091337136123906298L;

    public RentBikeException() {

    }

    public RentBikeException(String message) {

        super(message);

    }
}
