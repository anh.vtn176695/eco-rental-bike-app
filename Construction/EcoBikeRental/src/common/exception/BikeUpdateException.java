package common.exception;

/**
 * The BikeUpdateException wraps all unchecked exceptions You can use this exception to inform
 */

public class BikeUpdateException extends EcoRentalBikeException {

    private static final long serialVersionUID = 1091337136123906298L;

    public BikeUpdateException() {

    }

    public BikeUpdateException(String message) {

        super(message);

    }
}
