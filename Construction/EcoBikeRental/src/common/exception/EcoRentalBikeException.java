package common.exception;

/**
 * The EcoRentalBikeException wraps all unchecked exceptions You can use this exception to inform
 */

public class EcoRentalBikeException extends RuntimeException {

    public EcoRentalBikeException() {

    }

    public EcoRentalBikeException(String message) {
        super(message);
    }

}
