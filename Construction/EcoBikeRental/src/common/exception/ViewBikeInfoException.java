package common.exception;

/**
 * The ViewBikeInfoException wraps all unchecked exceptions You can use this exception to inform
 */

public class ViewBikeInfoException extends EcoRentalBikeException {

    private static final long serialVersionUID = 1091337136123906298L;

    public ViewBikeInfoException() {

    }

    public ViewBikeInfoException(String message) {

        super(message);

    }
}