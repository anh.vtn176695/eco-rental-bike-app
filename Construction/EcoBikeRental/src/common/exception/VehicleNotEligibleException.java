package common.exception;

/**
 * The BikeNotAvailableException wraps all unchecked exceptions You can use this exception to inform
 */

public class VehicleNotEligibleException extends EcoRentalBikeException {

    private static final long serialVersionUID = 1091337136123906298L;

    public VehicleNotEligibleException() {

    }

    public VehicleNotEligibleException(String message) {

        super(message);

    }

}
