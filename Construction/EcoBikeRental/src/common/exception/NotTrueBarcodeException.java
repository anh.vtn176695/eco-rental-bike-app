package common.exception;

/**
 * The NotTrueBarcodeException wraps all unchecked exceptions You can use this exception to inform
 */

public class NotTrueBarcodeException extends EcoRentalBikeException {

    public NotTrueBarcodeException(String message) {

        super(message);

    }

}
