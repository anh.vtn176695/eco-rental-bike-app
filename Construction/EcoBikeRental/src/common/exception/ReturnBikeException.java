package common.exception;

/**
 * The ReturnBikeException wraps all unchecked exceptions You can use this exception to inform
 */

public class ReturnBikeException extends EcoRentalBikeException {

    private static final long serialVersionUID = 1091337136123906298L;

    public ReturnBikeException() {

    }

    public ReturnBikeException(String message) {

        super(message);

    }

}
