package common.exception;

/**
 * The FullDockingPointException wraps all unchecked exceptions You can use this exception to inform
 */

public class FullDockingPointException extends EcoRentalBikeException {

    public FullDockingPointException(String message) {

        super(message);

    }

}