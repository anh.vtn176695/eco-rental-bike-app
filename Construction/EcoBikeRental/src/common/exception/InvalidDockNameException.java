package common.exception;

/**
 * The InvalidDockNameException wraps all unchecked exceptions You can use this exception to inform
 */

public class InvalidDockNameException extends EcoRentalBikeException {

    public InvalidDockNameException() {

        super("ERROR: Invalid the docking name ");

    }

}
