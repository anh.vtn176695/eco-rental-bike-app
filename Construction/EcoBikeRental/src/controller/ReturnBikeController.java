package controller;

import entity.station.Station;

import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * This class controls the flow of the return bike usecase in our EcoRentalBike app
 */

public class ReturnBikeController extends BaseController {

    private static Logger LOGGER = utils.Utils.getLogger(ReturnBikeController.class.getName());

    /**
     * This method chooses a station near your location
     * @throws SQLException
     */
    public void station() throws SQLException {

    }

    /**
     * This method checks the docking point when user enter the Station then press Ok button
     * @throws SQLException
     */
    public void returnBike() throws SQLException {
        Station.getStation().checkEmptyDockingPoint();
    }


}
