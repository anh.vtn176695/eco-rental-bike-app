package views.screen.returnBike;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import entity.rent.RentBike;
import entity.returnbike.ReturnBike;
import entity.station.Station;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import module.Database;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class EnterBarcode implements Initializable {

    @FXML
    private ImageView stationImage;

    @FXML
    private Label stationName;

    @FXML
    private Label stationLocation;

    @FXML
    private Label distance;

    @FXML
    private Label walkingTime;

    @FXML
    private ImageView cayXangIcon;

    @FXML
    private ImageView locationIcon;

    @FXML
    private ImageView distanceIcon;

    @FXML
    private ImageView bikeIcon;

    @FXML
    private ImageView walkingIcon;

    @FXML
    private JFXButton backButton;

    @FXML
    private Label numOfEmptyDockingPoint;

    @FXML
    private Label notifyErrorBarcode;

    @FXML
    private JFXTextField barCodeField;

    @FXML
    private JFXButton lockButton;

    @FXML
    private Pane paneRoot;

    public String nameStation;
    public String location;
    public String distances;
    public String walkingTimes;
    public int availableDockingPoint;
    public String barcode;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        File file1 = new File("assets/images/stationInfo.png");
        Image image1 = new Image(file1.toURI().toString());
        stationImage.setImage(image1);

        File file2 = new File("assets/images/cayXangIcon.png");
        Image image2 = new Image(file2.toURI().toString());
        cayXangIcon.setImage(image2);

        File file3 = new File("assets/images/locationIcon.png");
        Image image3 = new Image(file3.toURI().toString());
        locationIcon.setImage(image3);

        File file4 = new File("assets/images/distanceIcon.png");
        Image image4 = new Image(file4.toURI().toString());
        distanceIcon.setImage(image4);

        File file5 = new File("assets/images/bikeIcon.png");
        Image image5 = new Image(file5.toURI().toString());
        bikeIcon.setImage(image5);

        File file6 = new File("assets/images/dibo.png");
        Image image6 = new Image(file6.toURI().toString());
        walkingIcon.setImage(image6);

        try {
            getData();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        notifyErrorBarcode.setVisible(false);
        stationName.setText(nameStation);
        stationLocation.setText(location);
        numOfEmptyDockingPoint.setText(String.valueOf(availableDockingPoint));
        distance.setText(distances);
        walkingTime.setText(walkingTimes);
        barCodeField.setText(barcode);

        backButton.setOnAction(e -> {
            Parent root = null;
            try {
                root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/returnBike/selectStation.fxml"));
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            setNode(root);
        });
    }

        public void lockButtonOnAction(ActionEvent event) throws SQLException, ClassNotFoundException, IOException {
            String code = barCodeField.getText();
            boolean check = new ReturnBike().checkReturnBarcode(code);
            if (check) {
//                String barcode = new RentBike().getBarcode(code);
                insertSelectBike(code);
                Parent root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/returnBike/payment.fxml"));
                setNode(root);
            }
            else {
                notifyErrorBarcode.setVisible(true);
            }
        }


    public void setNode(Node node){
        paneRoot.getChildren().clear();
        paneRoot.getChildren().add(node);

        FadeTransition fadeTransition = new FadeTransition(Duration.millis(1500));
        fadeTransition.setNode(node);
        fadeTransition.setFromValue(0.1);
        fadeTransition.setToValue(1);
        fadeTransition.setCycleCount(1);
        fadeTransition.setAutoReverse(false);
        fadeTransition.play();
    }

    public void getData() throws SQLException, ClassNotFoundException {
        int id = new Station().getStationNow();
        Connection connection = new Database().getConnection();
        String sql = "select * from Station where id='"+id+"'";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        if (resultSet.next()) {
            nameStation = resultSet.getString("name");
            location = resultSet.getString("location");
            distances = resultSet.getString("distance");
            walkingTimes = resultSet.getString("walkingTime");
            availableDockingPoint = resultSet.getInt("numOfEmptyDockingPoint");
//            barcode = resultSet.getString("barcode");
//            System.out.println(nameStation+location+area+availableDockingPoint);
        }
    }

//    public boolean checkBarcode(String barcode){
//
//    }



    public void insertSelectBike(String barcode) throws SQLException, ClassNotFoundException {
        String sql = "insert into selectBike (bike) values ('"+barcode+"')";
        Connection connection = new Database().getConnection();
        Statement statement = connection.createStatement();
        int re = statement.executeUpdate(sql);
        statement.close();
    }

}
