package views.screen.returnBike;

import com.jfoenix.controls.JFXButton;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import module.Database;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class SelectStation implements Initializable {

    @FXML
    private JFXButton button1;

    @FXML
    private ImageView stationImage1;

    @FXML
    private JFXButton button4;

    @FXML
    private ImageView stationImage4;

    @FXML
    private JFXButton button2;

    @FXML
    private ImageView stationImage2;

    @FXML
    private JFXButton button3;

    @FXML
    private ImageView stationImage3;

    @FXML
    private JFXButton button5;

    @FXML
    private ImageView stationImage5;

    @FXML
    private JFXButton button6;

    @FXML
    private ImageView stationImage6;

    @FXML
    private Pane viewStationRoot;



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        File file1 = new File("assets/images/station1.png");
        Image i1 = new Image(file1.toURI().toString());
        stationImage1.setImage(i1);

        File file2 = new File("assets/images/station2.png");
        Image i2 = new Image(file2.toURI().toString());
        stationImage2.setImage(i2);

        File file3 = new File("assets/images/station3.png");
        Image i3 = new Image(file3.toURI().toString());
        stationImage3.setImage(i3);

        File file4 = new File("assets/images/station4.jpg");
        Image i4 = new Image(file4.toURI().toString());
        stationImage4.setImage(i4);

        File file5 = new File("assets/images/station5.jpg");
        Image i5 = new Image(file5.toURI().toString());
        stationImage5.setImage(i5);

        File file6 = new File("assets/images/station6.jpg");
        Image i6 = new Image(file6.toURI().toString());
        stationImage6.setImage(i6);
    }

    @FXML
    void station1Action(ActionEvent event) throws IOException, SQLException, ClassNotFoundException {
        Connection connection = new Database().getConnection();
        insertSelectStation(1,connection);
        Parent root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/returnBike/enterBarcode.fxml"));
        setNode(root);
    }

    @FXML
    void station2Action(ActionEvent event) throws SQLException, ClassNotFoundException, IOException {
        Connection connection = new Database().getConnection();
        insertSelectStation(2,connection);
        Parent root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/returnBike/enterBarcode.fxml"));
        setNode(root);
    }

    @FXML
    void station3Action(ActionEvent event) throws SQLException, ClassNotFoundException, IOException {
        Connection connection = new Database().getConnection();
        insertSelectStation(3,connection);
        Parent root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/returnBike/enterBarcode.fxml"));
        setNode(root);
    }

    @FXML
    void station4Action(ActionEvent event) throws SQLException, ClassNotFoundException, IOException {
        Connection connection = new Database().getConnection();
        insertSelectStation(4,connection);
        Parent root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/returnBike/enterBarcode.fxml"));
        setNode(root);
    }

    @FXML
    void station5Action(ActionEvent event) throws SQLException, ClassNotFoundException, IOException {
        Connection connection = new Database().getConnection();
        insertSelectStation(5,connection);
        Parent root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/returnBike/enterBarcode.fxml"));
        setNode(root);
    }

    @FXML
    void station6Action(ActionEvent event) throws SQLException, ClassNotFoundException, IOException {
        Connection connection = new Database().getConnection();
        insertSelectStation(6,connection);
        Parent root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/returnBike/enterBarcode.fxml"));
        setNode(root);
    }

    public void setNode(Node node){
        viewStationRoot.getChildren().clear();
        viewStationRoot.getChildren().add(node);

        FadeTransition fadeTransition = new FadeTransition(Duration.millis(1500));
        fadeTransition.setNode(node);
        fadeTransition.setFromValue(0.1);
        fadeTransition.setToValue(1);
        fadeTransition.setCycleCount(1);
        fadeTransition.setAutoReverse(false);
        fadeTransition.play();
    }

    public void loadFXML(String location) throws IOException {
        Parent root = (Parent) FXMLLoader.load(getClass().getResource(location));
        Stage stage = new Stage(StageStyle.UNDECORATED);
        stage.setScene(new Scene(root));
        stage.setX(250);
        stage.setY(80);
        stage.show();
    }

    public void insertSelectStation(int id , Connection connection) throws SQLException {
        String sql = "insert into selectStation(bike) values("+id+")";
        Statement statement = connection.createStatement();
        int re = statement.executeUpdate(sql);
        statement.close();
    }
}
