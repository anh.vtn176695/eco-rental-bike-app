package views.screen.returnBike;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import entity.db.EcoRentalBikeDB;
import entity.rent.RentBike;
import entity.returnbike.ReturnBike;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import module.Database;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalTime;
import java.util.ResourceBundle;

public class Payment implements Initializable {

    @FXML
    private ImageView typeIcon;

    @FXML
    private ImageView timeIcon;

    @FXML
    private ImageView depositIcon;

    @FXML
    private ImageView moneyIcon;

    @FXML
    private Label type;

    @FXML
    private Label deposit;

    @FXML
    private Label time;

    @FXML
    private Label barcode;

    @FXML
    private Label amount;

    @FXML
    private Label notifySuccessfulPayment;

    @FXML
    private JFXTextField cardNumber;

    @FXML
    private JFXTextField cardHolderName;

    @FXML
    private JFXTextField expirationDate;

    @FXML
    private JFXTextField securityCode;

    @FXML
    private JFXButton backButton;

    @FXML
    private JFXButton paymentButton;

    @FXML
    private Pane paneRoot;

    public String types;
    public String deposits;
    public String times;
    public String amounts;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        File file1 = new File("assets/images/time.png");
        Image image1 = new Image(file1.toURI().toString());
        timeIcon.setImage(image1);

        File file2 = new File("assets/images/typeBike.jpg");
        Image image2 = new Image(file2.toURI().toString());
        typeIcon.setImage(image2);

        File file3 = new File("assets/images/deposit.png");
        Image image3 = new Image(file3.toURI().toString());
        depositIcon.setImage(image3);

        File file4 = new File("assets/images/money.png");
        Image image4 = new Image(file4.toURI().toString());
        moneyIcon.setImage(image4);

        try {
            getData();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        amount.setText(amounts);
        type.setText(types);
        time.setText(times);
        deposit.setText(deposits);

        backButton.setOnAction(e->{
            Parent root = null;
            try {
                root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/returnBike/enterBarcode.fxml"));
                setNode(root);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            } });

//        paymentButton
    }

    public void setNode(Node node){
        paneRoot.getChildren().clear();
        paneRoot.getChildren().add(node);

        FadeTransition fadeTransition = new FadeTransition(Duration.millis(1500));
        fadeTransition.setNode(node);
        fadeTransition.setFromValue(0.1);
        fadeTransition.setToValue(1);
        fadeTransition.setCycleCount(1);
        fadeTransition.setAutoReverse(false);
        fadeTransition.play();
    }

    public void getData() throws SQLException, ClassNotFoundException {
        Connection connection = new Database().getConnection();
        Statement statement = connection.createStatement();
        String getBarcodeSql = "select *from selectBike";
        String code="";
        ResultSet resultSet = statement.executeQuery(getBarcodeSql);
        while (resultSet.next()) {
            code =resultSet.getString("bike");
        }

        barcode.setText(code);
        String getType="select * from Bike where barcode='"+code+"'";
        resultSet = statement.executeQuery(getType);
        if (resultSet.next()) {
            types = resultSet.getString("type");
        }

        String sql = "select * from rentBike where barcode='"+code+"'";
        resultSet = statement.executeQuery(sql);
        if (resultSet.next()) {
            times="0";
            amounts = "0";
            deposits = resultSet.getString("deposit");
        }
    }
}
