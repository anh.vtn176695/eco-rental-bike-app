package views.screen;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import entity.rent.RentBike;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import module.Database;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class RentBikeBarcode implements Initializable {

    @FXML
    private ImageView rentBikeImage;

    @FXML
    private Label notifyWrongBarcode;

    @FXML
    private JFXTextField enterBarcodeBox;

    @FXML
    private JFXButton OkButton;

    @FXML
    private AnchorPane paneRoot;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        File file1 = new File("assets/images/rentBike.png");
        Image image1 = new Image(file1.toURI().toString());
        rentBikeImage.setImage(image1);

        notifyWrongBarcode.setVisible(false);
    }

    public void okButtonOnAction(ActionEvent event) throws SQLException, ClassNotFoundException, IOException {
        String code = enterBarcodeBox.getText();
        boolean check = new RentBike().validateRentBikeBarcode(code);
        if (check){
            notifyWrongBarcode.setVisible(false);
            String typeBike = new RentBike().getTypeOfBikeByBarcode(code);
            if (typeBike.equals("standardBike")) {
                insertSelectBike(code);
                Parent root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/standardBikeInfo.fxml"));
                setNode(root);
            }else if (typeBike.equals("eBike")){
                insertSelectBike(code);
                Parent root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/eBikeInfo.fxml"));
                setNode(root);
            }else{
                insertSelectBike(code);
                Parent root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/twinBikeInfo.fxml"));
                setNode(root);
            }
        }
        else {
            notifyWrongBarcode.setVisible(true);
        }
    }

    public void insertSelectBike(String code) throws SQLException, ClassNotFoundException {
        Connection connection = new Database().getConnection();
        Statement statement = connection.createStatement();
        String sql = "insert into selectBike(bike) values('"+code+"')";
        int re = statement.executeUpdate(sql);
    }

    public void setNode(Node node){
        paneRoot.getChildren().clear();
        paneRoot.getChildren().add(node);

        FadeTransition fadeTransition = new FadeTransition(Duration.millis(1500));
        fadeTransition.setNode(node);
        fadeTransition.setFromValue(0.1);
        fadeTransition.setToValue(1);
        fadeTransition.setCycleCount(1);
        fadeTransition.setAutoReverse(false);
        fadeTransition.play();
    }
}
