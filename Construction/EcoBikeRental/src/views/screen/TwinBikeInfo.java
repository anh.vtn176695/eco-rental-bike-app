package views.screen;

import com.jfoenix.controls.JFXButton;
import entity.bike.StandardBike;
import entity.bike.TwinBike;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import module.Database;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class TwinBikeInfo implements Initializable {

    @FXML
    private Pane paneRoot;

    @FXML
    private ImageView stationImage;

    @FXML
    private Label type;

    @FXML
    private Label deposit;

    @FXML
    private Label saddle;

    @FXML
    private Label pedal;

    @FXML
    private Label rearSeat;

    @FXML
    private Label successfulRent;

    @FXML
    private JFXButton backButton;

    @FXML
    private JFXButton rentBike;

    @FXML
    private Label barcode;

    private int bikeId;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        File file1 = new File("assets/images/twinBike.jpg");
        Image image1 = new Image(file1.toURI().toString());
        stationImage.setImage(image1);

        successfulRent.setVisible(false);

        backButton.setOnAction(e->{
            try {
                Parent root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/rentBikeBarcode.fxml"));
                setNode(root);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        String code="";
        try {
            code =getBarcodeNow();
            TwinBike bike = new TwinBike().getTwinBikeByBarcode(code);
            bikeId=bike.getId();
            type.setText(bike.getType());
            rearSeat.setText(bike.getRearSeat());
            deposit.setText(bike.getDeposit());
            saddle.setText(bike.getSaddle());
            pedal.setText(bike.getPedal());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        barcode.setText(code);
    }
    public String getBarcodeNow() throws SQLException, ClassNotFoundException {
        Connection connection = new Database().getConnection();
        Statement statement = connection.createStatement();
        String sql = "select * from selectBike";
        ResultSet ResultSet = statement.executeQuery(sql);
        String barcode="";
        while (ResultSet.next()){
            barcode = ResultSet.getString("bike");
        }
        return barcode;
    }

    public void rentBikeOnAction(ActionEvent event) throws SQLException, ClassNotFoundException, IOException {

        Connection connection = new Database().getConnection();
        Statement statement = connection.createStatement();
//        System.out.println(barcode.getText());
        String checkRentBikeSql = "select * from rentBike where barcode='"+barcode.getText()+"'";
        ResultSet resultSet = statement.executeQuery(checkRentBikeSql);
        boolean check = true;
        if (resultSet.next()) {check = false;}
        if (check) {
            String sql = "insert into rentBike(barcode, deposit, bikeId) values ('" + barcode.getText() + "', '" + deposit.getText() + "','" + bikeId + "')";
            int re = statement.executeUpdate(sql);
            successfulRent.setStyle("-fx-text-fill: #076711");
            successfulRent.setText("Rent bike successfully !");
            successfulRent.setVisible(true);
        }
        else{
            successfulRent.setStyle("-fx-text-fill: #FC0000");
            successfulRent.setText("This bike has been rented");
            successfulRent.setVisible(true);
        }
    }

    public void setNode(Node node){
        paneRoot.getChildren().clear();
        paneRoot.getChildren().add(node);

        FadeTransition fadeTransition = new FadeTransition(Duration.millis(1500));
        fadeTransition.setNode(node);
        fadeTransition.setFromValue(0.1);
        fadeTransition.setToValue(1);
        fadeTransition.setCycleCount(1);
        fadeTransition.setAutoReverse(false);
        fadeTransition.play();
    }
}
