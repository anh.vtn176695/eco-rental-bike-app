package views.screen;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXHamburger;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HomeScreen implements Initializable {

    @FXML
    private ImageView logoImage;

    @FXML
    private ImageView logoText;

    @FXML
    private JFXButton aboutUsButton;

    @FXML
    private JFXButton viewBikeButton;

    @FXML
    private JFXButton rentBikeButton;

    @FXML
    private JFXButton returnBikeButton;

    @FXML
    private JFXButton cancelButton;

    @FXML
    private ImageView searchIcon;

    @FXML
    private Label titleLabel;

    @FXML
    private AnchorPane anchorPaneRoot;

    Pane root1;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        File file1 = new File("assets/images/logoEconike.png");
        Image ImageLogoImage = new Image(file1.toURI().toString());
        logoImage.setImage(ImageLogoImage);

        File file2 = new File("assets/images/textEcobike.png");
        Image textEcobikeImage = new Image(file2.toURI().toString());
        logoText.setImage(textEcobikeImage);

        File file3 = new File("assets/images/searchImage.png");
        Image searchIconImage = new Image(file3.toURI().toString());
        searchIcon.setImage(searchIconImage);

        titleLabel.setText("Welcome");

        try {
            root1 = (Pane) FXMLLoader.load(getClass().getResource("/views/fxml/viewStation.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setNode(root1);

        cancelButton.setOnMouseClicked(e->{
            Stage s = (Stage) cancelButton.getScene().getWindow();
            s.close();
        });

        viewBikeButton.setOnAction(event->{
            viewBikeButton.setStyle("-fx-background-color:#ffffff;");
            try {
                root1 = (Pane) FXMLLoader.load(getClass().getResource("/views/fxml/viewStation.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            setNode(root1);
            rentBikeButton.setStyle("-fx-background-color:#C5C9C4;");
            returnBikeButton.setStyle("-fx-background-color:#C5C9C4;");
            titleLabel.setText("View Bike");
        });

        returnBikeButton.setOnMouseClicked(e->{
            returnBikeButton.setStyle("-fx-background-color:#ffffff;");
            viewBikeButton.setStyle("-fx-background-color: #C5C9C4;");
            rentBikeButton.setStyle("-fx-background-color:#C5C9C4;");
            anchorPaneRoot.getChildren().clear();
            titleLabel.setText("Select a station to return bike");
            try {
                root1 = (Pane) FXMLLoader.load(getClass().getResource("/views/fxml/returnBike/selectStation.fxml"));
                setNode(root1);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        rentBikeButton.setOnMouseClicked(e->{
            returnBikeButton.setStyle("-fx-background-color:#C5C9C4;");
            viewBikeButton.setStyle("-fx-background-color: #C5C9C4;");
            rentBikeButton.setStyle("-fx-background-color:#ffffff;");
            anchorPaneRoot.getChildren().clear();
            titleLabel.setText("Rent Bike");
            try {
                root1 = (Pane) FXMLLoader.load(getClass().getResource("/views/fxml/rentBikeBarcode.fxml"));
                setNode(root1);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });
    }


    public void goToHomePage() throws IOException {
        Parent root1 = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/homeScreen.fxml"));
        Scene scene1 = new Scene(root1);
        Stage windowLogin = new Stage();
        windowLogin.setScene(scene1);
//        windowLogin.setTitle("Eco Bike");
        windowLogin.initStyle(StageStyle.UNDECORATED);
        windowLogin.show();
    }

    public void setNode(Node node){
        anchorPaneRoot.getChildren().clear();
        anchorPaneRoot.getChildren().add((Node) node);

        FadeTransition fadeTransition = new FadeTransition(Duration.millis(1500));
        fadeTransition.setNode(node);
        fadeTransition.setFromValue(0.1);
        fadeTransition.setToValue(1);
        fadeTransition.setCycleCount(1);
        fadeTransition.setAutoReverse(false);
        fadeTransition.play();
    }
}
