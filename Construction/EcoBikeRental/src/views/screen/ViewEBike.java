package views.screen;

import com.jfoenix.controls.JFXButton;
import entity.bike.EBike;
import entity.station.Station;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ViewEBike implements Initializable {

    @FXML
    private AnchorPane paneRoot;

    @FXML
    private TableView<EBike> table;

    @FXML
    private TableColumn<EBike, Integer> id;

    @FXML
    private TableColumn<EBike, String> type;

    @FXML
    private TableColumn<EBike, String> saddle;

    @FXML
    private TableColumn<EBike, String> barcode;

    @FXML
    private TableColumn<EBike, String> deposit;

    @FXML
    private TableColumn<EBike, String> rearSeat;

    @FXML
    private TableColumn<EBike, String> pedal;

    @FXML
    private TableColumn<EBike, String> batteryPercentage;

    @FXML
    private TableColumn<EBike, String> leftTime;


    @FXML
    private JFXButton backButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        backButton.setOnAction(e->{
            Parent root = null;
            try {
                root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/stationInformation.fxml"));
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            setNode(root);
        });

        id.setCellValueFactory(new PropertyValueFactory<EBike, Integer>("id"));
        type.setCellValueFactory(new PropertyValueFactory<EBike, String>("type"));
        saddle.setCellValueFactory(new PropertyValueFactory<EBike, String>("saddle"));
        barcode.setCellValueFactory(new PropertyValueFactory<EBike, String>("barcode"));
        deposit.setCellValueFactory(new PropertyValueFactory<EBike, String>("deposit"));
        rearSeat.setCellValueFactory(new PropertyValueFactory<EBike, String>("rearSeat"));
        pedal.setCellValueFactory(new PropertyValueFactory<EBike, String>("pedal"));
        batteryPercentage.setCellValueFactory(new PropertyValueFactory<EBike, String>("batteryPercentage"));
        leftTime.setCellValueFactory(new PropertyValueFactory<EBike, String>("leftTime"));

        int stationId=0;
        try {
            stationId = new Station().getStationNow();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            table.setItems(new EBike().getAllEBikeByStationId(stationId));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void setNode(Node node){
        paneRoot.getChildren().clear();
        paneRoot.getChildren().add((Node) node);

        FadeTransition fadeTransition = new FadeTransition(Duration.millis(1500));
        fadeTransition.setNode(node);
        fadeTransition.setFromValue(0.1);
        fadeTransition.setToValue(1);
        fadeTransition.setCycleCount(1);
        fadeTransition.setAutoReverse(false);
        fadeTransition.play();
    }
}
