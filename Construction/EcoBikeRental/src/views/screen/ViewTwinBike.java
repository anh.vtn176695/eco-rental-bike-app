package views.screen;

import com.jfoenix.controls.JFXButton;
import entity.bike.StandardBike;
import entity.bike.TwinBike;
import entity.station.Station;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ViewTwinBike implements Initializable {

    @FXML
    private AnchorPane paneRoot;

    @FXML
    private TableView<TwinBike> table;

    @FXML
    private TableColumn<TwinBike, Integer> id;

    @FXML
    private TableColumn<TwinBike, String> type;

    @FXML
    private TableColumn<TwinBike, Integer> saddle;

    @FXML
    private TableColumn<TwinBike, String> barcode;

    @FXML
    private TableColumn<TwinBike, Integer> deposit;

    @FXML
    private TableColumn<TwinBike, Integer> rearSeat;

    @FXML
    private TableColumn<TwinBike, Integer> pedal;

    @FXML
    private JFXButton backButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        backButton.setOnAction(e->{
            try {
                Parent root = (Parent) FXMLLoader.load(getClass().getResource("/views/fxml/stationInformation.fxml"));
                setNode(root);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        id.setCellValueFactory(new PropertyValueFactory<TwinBike, Integer>("id"));
        type.setCellValueFactory(new PropertyValueFactory<TwinBike, String>("type"));
        saddle.setCellValueFactory(new PropertyValueFactory<TwinBike, Integer>("saddle"));
        barcode.setCellValueFactory(new PropertyValueFactory<TwinBike, String>("barcode"));
        deposit.setCellValueFactory(new PropertyValueFactory<TwinBike, Integer>("deposit"));
        rearSeat.setCellValueFactory(new PropertyValueFactory<TwinBike, Integer>("rearSeat"));
        pedal.setCellValueFactory(new PropertyValueFactory<TwinBike, Integer>("pedal"));

        try {
            table.setItems(new TwinBike().getAllTwinBike(new Station().getStationNow()));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }




    public void setNode(Node node){
        paneRoot.getChildren().clear();
        paneRoot.getChildren().add((Node) node);

        FadeTransition fadeTransition = new FadeTransition(Duration.millis(1500));
        fadeTransition.setNode(node);
        fadeTransition.setFromValue(0.1);
        fadeTransition.setToValue(1);
        fadeTransition.setCycleCount(1);
        fadeTransition.setAutoReverse(false);
        fadeTransition.play();
    }
}
