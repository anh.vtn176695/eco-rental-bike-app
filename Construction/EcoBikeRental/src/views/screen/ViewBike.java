package views.screen;

import com.jfoenix.controls.JFXButton;
import entity.bike.Bike;
import entity.bike.StandardBike;
import entity.db.EcoRentalBikeDB;
import entity.station.Station;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import module.Database;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class ViewBike implements Initializable {

    @FXML
    private AnchorPane paneRoot;

    @FXML
    private JFXButton backButton;

    @FXML
    private TableView<StandardBike> table;

    @FXML
    private TableColumn<StandardBike, Integer> id;

    @FXML
    private TableColumn<StandardBike, String> type;

    @FXML
    private TableColumn<StandardBike, String> saddle;

    @FXML
    private TableColumn<StandardBike, String> barcode;

    @FXML
    private TableColumn<StandardBike, String> deposit;

    @FXML
    private TableColumn<StandardBike, String> rearSeat;

    @FXML
    private TableColumn<StandardBike, String> pedal;


//    ObservableList<Bike> list1 = FXCollections.observableArrayList(
//            new Bike(1,"Bike","Bike", 1,1,1,"barcode")
//    );

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        id.setCellValueFactory(new PropertyValueFactory<StandardBike, Integer>("id"));
        type.setCellValueFactory(new PropertyValueFactory<StandardBike, String>("type"));
        saddle.setCellValueFactory(new PropertyValueFactory<StandardBike, String>("saddle"));
        barcode.setCellValueFactory(new PropertyValueFactory<StandardBike, String>("barcode"));
        deposit.setCellValueFactory(new PropertyValueFactory<StandardBike, String>("deposit"));
        rearSeat.setCellValueFactory(new PropertyValueFactory<StandardBike, String>("rearSeat"));
        pedal.setCellValueFactory(new PropertyValueFactory<StandardBike, String>("pedal"));

//        table.setItems(list1);
        try {
            table.setItems(getAllBike());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        backButton.setOnAction(e->{
            try {
                Parent root =(Parent) FXMLLoader.load(getClass().getResource("/views/fxml/stationInformation.fxml"));
                setNode(root);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });
    }

    public ObservableList<StandardBike> getAllBike() throws ClassNotFoundException, SQLException {
        int id = new Station().getStationNow();
        ObservableList<StandardBike> list = FXCollections.observableArrayList();
        Connection connection = new Database().getConnection();
        Statement statement = connection.createStatement();
        String sql = "select * from Bike where type = 'standardBike' and stationId='"+id+"'";
        ResultSet resultSet = statement.executeQuery(sql);
        int bikeId=0;
        while (resultSet.next()){
//            int bikeId = resultSet.getInt(1);
            bikeId++;
            String type = resultSet.getString(2);
            String saddle =resultSet.getString(3);
            String pedal = resultSet.getString(4);
            String barcode = resultSet.getString(5);
            String deposit  = resultSet.getString(6);
            String rearSeat = resultSet.getString(7);
            int stationId = resultSet.getInt(8);

            Statement stm= EcoRentalBikeDB.getConnection().createStatement();
            list.add(new StandardBike(stm,bikeId, type, saddle,barcode, deposit,rearSeat, pedal,stationId));
        }
        return list;
    }


    public void setNode(Node node){
        paneRoot.getChildren().clear();
        paneRoot.getChildren().add((Node) node);

        FadeTransition fadeTransition = new FadeTransition(Duration.millis(1500));
        fadeTransition.setNode(node);
        fadeTransition.setFromValue(0.1);
        fadeTransition.setToValue(1);
        fadeTransition.setCycleCount(1);
        fadeTransition.setAutoReverse(false);
        fadeTransition.play();
    }
}
