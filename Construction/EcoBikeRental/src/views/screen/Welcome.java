package views.screen;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Welcome implements Initializable {

    @FXML
    private ImageView welcome;

    @FXML
    private JFXButton closeButton;

    @FXML
    private JFXButton minimizeButton;

    public void initialize(URL location, ResourceBundle resources) {
        File file = new File("assets/images/welcome.png");
        Image imageWelcome = new Image(file.toURI().toString());
        welcome.setImage(imageWelcome);

        welcome.setOnMouseClicked(e-> {
            Stage s = (Stage) welcome.getScene().getWindow();
            s.close();
            HomeScreen homeScreen = new HomeScreen();
            try {
                homeScreen.goToHomePage();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        closeButton.setOnMouseClicked(e->{
            Stage s = (Stage) closeButton.getScene().getWindow();
            s.close();
        });

        minimizeButton.setOnMouseClicked(e->{
            ((Stage)((Button)e.getSource()).getScene().getWindow()).setIconified(true);
        });
    }

    public void goToWelcomePage(Stage pStage) throws IOException {
        StackPane root1 = (StackPane) FXMLLoader.load(getClass().getResource("/views/fxml/welcome.fxml"));
        Scene scene1 = new Scene(root1);
        pStage.setScene(scene1);
        pStage.setTitle("Eco Bike");
        pStage.show();
    }
}
